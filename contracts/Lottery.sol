pragma solidity ^0.4.24;

contract Lottery {
    address public manager;
    address[] private players;

    constructor() public {
        manager = msg.sender;
    }

    /**
     * enter the lottery
     */
    function enter() public payable {
        require(msg.value > (0.1 ether), "Please, send more than 0.1 ether");
        players.push(msg.sender);
    }

    /**
    There is no such thing as true randomness in Solidity
     */
    function random() private view returns (uint) {
        // abi.encodePacked(players)
        return uint(keccak256(block.difficulty, block.timestamp));
    }

    /**
    Only the manager should be able to call this method
    */
    function pickWinner() public restricted {
        uint index = random() % players.length;
        players[index].transfer(this.balance);
        // Reset the players array, so we are ready for the next round
        players = new address[](0);
    }

    // TODO: I could also use the ownerOnly modified which I've defined in previous learning project
    modifier restricted() {
        require(msg.sender == manager, "Only manager is allowed to run this method");
        _;
    }

    function getPlayers() public view returns (address[]) {
        return players;
    }
}