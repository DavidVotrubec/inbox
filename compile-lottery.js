const path = require('path');
const fs = require('fs');
const solc = require('solc');

const inboxPath = path.resolve(__dirname, 'contracts', 'Lottery.sol');
const source = fs.readFileSync(inboxPath, 'utf8');

// Make the output of compilation immediatelly available by exporting it
// (we are insterested in the Lottery only)
const compilationOutput = solc.compile(source, 1);

console.log('compilation output - interface', compilationOutput.contracts[':Lottery'].interface);
console.log('compilation output - bytecode', compilationOutput.contracts[':Lottery'].bytecode);

// module.exports is for node.js
// import is done via require()
module.exports = compilationOutput.contracts[':Lottery'];