const path = require('path');
const fs = require('fs');
const solc = require('solc');

const inboxPath = path.resolve(__dirname, 'contracts', 'Inbox.sol');
const source = fs.readFileSync(inboxPath, 'utf8');

// Make the output of compilation immediatelly available by exporting it
// (we are insterested in the Inbox only)
module.exports = solc.compile(source, 1).contracts[':Inbox'];