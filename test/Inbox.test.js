const assert = require('assert'); // built in node.js
const ganache = require('ganache-cli');
const Web3 = require('web3'); // constructor, will make instances, therefore it is in uppercase
const { interface, bytecode } = require('../compile-inbox');

const provider = ganache.provider();
const web3 = new Web3(provider);

let accounts;
let inbox;

beforeEach(async() => {
    // Get a list of all accounts
    accounts = await web3.eth.getAccounts();
    // console.log('accounts', accounts);

    // use one of those accounts to deploy the contract
    inbox = await new web3.eth.Contract(JSON.parse(interface))
            .deploy({
                data: bytecode,
                arguments: ['Hi, this is the initial string']
            })
            .send({
                from: accounts[0],
                gas: '1000000'
            });

    // See https://www.udemy.com/ethereum-and-solidity-the-complete-developers-guide/learn/v4/t/lecture/9042518?start=0 for more details
    inbox.setProvider(provider);
});

describe('Inbox', () => {
    it('deploys a contract', () => {
        assert.ok(inbox.options.address);
    });

    it('has a default message', async () => {
        const message = await inbox.methods.message().call();
        assert.equal(message, 'Hi, this is the initial string');
    });

    it('can change the message', async () => {
        await inbox.methods.setMessage('new message').send({
            from: accounts[0],
            gas: '1000000'
        });
        const message = await inbox.methods.message().call();
        assert.equal(message, 'new message');
    });
});
