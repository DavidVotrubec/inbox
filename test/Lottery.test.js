const assert = require('assert'); // built in node.js
const ganache = require('ganache-cli');
const Web3 = require('web3'); // constructor, will make instances, therefore it is in uppercase
const { interface, bytecode } = require('../compile-lottery');

const provider = ganache.provider();
const web3 = new Web3(provider);

async function assertThrowsAsync(fn, regExp) {
    let f = () => {};
    try {
      await fn();
    } catch(e) {
      f = () => {throw e};
    } finally {
      assert.throws(f, regExp);
    }
  }


let accounts;
let lottery;

beforeEach(async() => {
    // Get a list of all accounts
    accounts = await web3.eth.getAccounts();
    // console.log('accounts', accounts);

    // use one of those accounts to deploy the contract
    lottery = await new web3.eth.Contract(JSON.parse(interface))
            .deploy({
                data: bytecode
            })
            .send({
                from: accounts[0],
                gas: '1000000'
            });

    // See https://www.udemy.com/ethereum-and-solidity-the-complete-developers-guide/learn/v4/t/lecture/9042518?start=0 for more details
    lottery.setProvider(provider);
});

describe('Lottery', () => {
    it('deploys a contract', () => {
        assert.ok(lottery.options.address);
    });

    it('allows to enter', async () => {
        await lottery.methods.enter().send({
            from: accounts[1],
            value: web3.utils.toWei('0.2', 'ether') // more than 0.1 is required
        });

        const participants = await lottery.methods.getPlayers().call({
            from: accounts[1]
        });
        assert.equal(true, participants.includes(accounts[1]));
    });

    it('does not allow to enter', async () => {
        try {
            await lottery.methods.enter().send({
                from: accounts[1],
                value: web3.utils.toWei('0.1', 'ether') // 0.1 is not enough to enter
            });
            assert(false);
        } catch (err) {
            assert(err);
        }
    });

    it('allows to enter multiple times', async () => {
        await lottery.methods.enter().send({
            from: accounts[1],
            value: web3.utils.toWei('0.3', 'ether')
        });

        await lottery.methods.enter().send({
            from: accounts[1],
            value: web3.utils.toWei('0.3', 'ether')
        });

        const participants = await lottery.methods.getPlayers().call({
            from: accounts[1]
        });
        const matches = participants.filter(p => p == accounts[1]);
        assert.equal(2, matches.length);
    });

    it('should allow only manager', async () => {
        await lottery.methods.enter().send({
            from: accounts[1],
            value: web3.utils.toWei('0.3', 'ether')
        });

        await lottery.methods.pickWinner().send({
            from: accounts[0]
        });
        assert(true);
    });

    it('should not allow non manager', async () => {
        try {
            await lottery.methods.pickWinner().send({
                from: accounts[1]
            });
            assert(false);
        }
        catch (err) {
            assert(err);
        }
    });

    it('should send money to winner and reset players array', async () => {
        const startBalance = web3.eth.getBalance(accounts[1]);

        await lottery.methods.enter().send({
            from: accounts[1],
            value: web3.utils.toWei('2', 'ether')
        });

        const afterBalance = await web3.eth.getBalance(accounts[1]);

        await lottery.methods.pickWinner().send({
            from: accounts[0]
        });

        // The final balance on winner's account should be about the same as initial
        // and about 2 ETH different from the 'after balance'
        // Some fees were charged for the gas costs
        const endBalance = await web3.eth.getBalance(accounts[1]);
        const difference = endBalance - afterBalance;
        assert(difference > web3.utils.toWei('1.95', 'ether'));

        // and yes, I should add asset for empty players array
    });
});
